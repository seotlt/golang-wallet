package main

import (
	"encoding/json"
	"log"
	"time"

	"gitlab.com/seotlt/golang_bch/protocol/helper"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"github.com/Equanox/gotron"
	"gitlab.com/seotlt/golang_bch/controller"
	"gitlab.com/seotlt/golang_bch/protocol"
)

func main() {
	helper.ReadConfig()
	protocol.InitLatestBlock()
	// Create a new browser window instance
	window, err := gotron.New("ui/dist")
	if err != nil {
		panic(err)
	}

	// Alter default window size and window title.
	window.WindowOptions.Width = 1200
	window.WindowOptions.Height = 980
	window.WindowOptions.Title = "Gotron boilerplate"

	// Start the browser window.
	// This will establish a golang <=> nodejs bridge using websockets,
	// to control ElectronBrowserWindow with our window object.
	done, err := window.Start()
	if err != nil {
		panic(err)
	}

	if _, _, err := controller.StartNode(true); err != nil {
		log.Println(err)
	}

	type createWallet struct {
		EvName string `json:"event"`
		Pwd    string `json:"password"`
	}
	window.On(&gotron.Event{Event: "createWallet"}, func(bin []byte) {
		var data createWallet
		json.Unmarshal(bin, &data)

		controller.GetNewWallet([]byte(data.Pwd))
	})

	type importWalletStruct struct {
		EvName  string `json:"event"`
		PrivKey string `json:"privkey"`
		Pwd     string `json:"password"`
	}
	window.On(&gotron.Event{Event: "importWallet"}, func(bin []byte) {
		var data importWalletStruct
		json.Unmarshal(bin, &data)

		var errStr string
		_, err := controller.ImportWallet(data.PrivKey, []byte(data.Pwd))
		if err != nil {
			errStr = err.Error()
		}

		type importWallEv struct {
			*gotron.Event
			Err string `json:"err"`
		}

		window.Send(&importWallEv{
			Event: &gotron.Event{Event: "importWall"},
			Err:   errStr,
		})
	})

	window.On(&gotron.Event{Event: "getWallets"}, func(bin []byte) {
		walletsFiles, err := controller.GetAllWallets()
		if err != nil {
			log.Println(err)
		}

		type walletInf struct {
			Address  string `json:"address"`
			Filename string `json:"filename"`
		}

		var wallets []walletInf
		for _, ks := range walletsFiles {
			ksFilename := ks.Name()
			address := ksFilename[:len(ksFilename)-5]

			wallet := walletInf{
				Address:  address,
				Filename: ksFilename,
			}

			wallets = append(wallets, wallet)
		}

		type walletsEv struct {
			*gotron.Event
			Wallets []walletInf `json:"wallets"`
		}
		window.Send(&walletsEv{
			Event:   &gotron.Event{Event: "wallets"},
			Wallets: wallets,
		})
	})

	type unlockWalletStruct struct {
		EvName string `json:"event"`
		FName  string `json:"filename"`
		Pwd    string `json:"password"`
	}
	window.On(&gotron.Event{Event: "unlockWallet"}, func(bin []byte) {
		var data unlockWalletStruct
		json.Unmarshal(bin, &data)

		var errStr string
		err := controller.UnlockWallet(data.FName, []byte(data.Pwd))
		if err != nil {
			errStr = err.Error()
		}

		type unlockWallEv struct {
			*gotron.Event
			Address string `json:"address"`
			Err     string `json:"err"`
		}

		if err := window.Send(&unlockWallEv{
			Event:   &gotron.Event{Event: "unlockWall"},
			Address: data.FName[:len(data.FName)-5],
			Err:     errStr,
		}); err != nil {
			log.Println(err)
		}
	})

	type deleteWalletStruct struct {
		EvName string `json:"event"`
		FName  string `json:"filename"`
	}
	window.On(&gotron.Event{Event: "deleteWallet"}, func(bin []byte) {
		var data deleteWalletStruct
		json.Unmarshal(bin, &data)

		controller.DeleteWallet(data.FName)
	})

	window.On(&gotron.Event{Event: "getPrivateKey"}, func(bin []byte) {
		type privateKeyEv struct {
			*gotron.Event
			PrivateKey string `json:"privateKey"`
		}

		currWallet, err := wallet.GetCurrentWallet()
		if err != nil {
			log.Println(err)
		}

		if err := window.Send(&privateKeyEv{
			Event:      &gotron.Event{Event: "privateKey"},
			PrivateKey: currWallet.PrivateKey,
		}); err != nil {
			log.Println(err)
		}
	})

	window.On(&gotron.Event{Event: "startRPC"}, func(bin []byte) {
		port := controller.StartRPC()

		type rpcEv struct {
			*gotron.Event
			Port string `json:"port"`
		}
		if err := window.Send(&rpcEv{
			Event: &gotron.Event{Event: "rpc"},
			Port:  port,
		}); err != nil {
			log.Println(err)
		}
	})

	type getBalanceStruct struct {
		EvName  string `json:"event"`
		Type    int8   `json:"type"`
		TkName  string `json:"tkname"`
		Address string `json:"address"`
	}
	window.On(&gotron.Event{Event: "getBalance"}, func(bin []byte) {
		var data getBalanceStruct
		json.Unmarshal(bin, &data)

		balance := controller.GetBalance(data.Type, data.TkName, data.Address)

		type balanceEv struct {
			*gotron.Event
			Balance int `json:"balance"`
		}
		if data.TkName == "" {
			window.Send(&balanceEv{
				Event:   &gotron.Event{Event: "balance"},
				Balance: balance,
			})
		} else {
			window.Send(&balanceEv{
				Event:   &gotron.Event{Event: "tokenbalance"},
				Balance: balance,
			})
		}
	})

	type sendTXStruct struct {
		EvName string            `json:"event"`
		Type   int8              `json:"type"`
		To     string            `json:"to"`
		Params map[string]string `json:"params"`
	}
	window.On(&gotron.Event{Event: "sendTX"}, func(bin []byte) {
		var data sendTXStruct
		json.Unmarshal(bin, &data)

		var errStr string
		hash, err := controller.SendNewTX(data.Type, data.To, data.Params)
		if err != nil {
			errStr = err.Error()
		}

		type sendTXEv struct {
			*gotron.Event
			Hash string `json:"hash"`
			Err  string `json:"err"`
		}
		window.Send(&sendTXEv{
			Event: &gotron.Event{Event: "sendtx"},
			Hash:  hash,
			Err:   errStr,
		})
	})

	type getUserTokensStruct struct {
		EvName  string `json:"event"`
		Address string `json:"address"`
	}
	window.On(&gotron.Event{Event: "getUserTokens"}, func(bin []byte) {
		var data getUserTokensStruct
		json.Unmarshal(bin, &data)

		tokens := controller.GetUserTokens(data.Address)

		type userTokensEv struct {
			*gotron.Event
			Tokens map[string]string `json:"tokens"`
		}
		window.Send(&userTokensEv{
			Event:  &gotron.Event{Event: "userTokens"},
			Tokens: tokens,
		})
	})

	type getTokenInfoStruct struct {
		EvName string `json:"event"`
		TkName string `json:"tkName"`
	}
	window.On(&gotron.Event{Event: "getTokenInfo"}, func(bin []byte) {
		var data getTokenInfoStruct
		json.Unmarshal(bin, &data)

		var errStr string
		tkInfo, err := controller.GetTokenInfo(data.TkName)
		if err != nil {
			errStr = err.Error()
		}

		type tokenInfoEv struct {
			*gotron.Event
			TkName string             `json:"tkName"`
			TkInfo protocol.TokenInfo `json:"tkInfo"`
			Err    string             `json:"err"`
		}
		window.Send(&tokenInfoEv{
			Event:  &gotron.Event{Event: "tkInfo"},
			TkName: data.TkName,
			TkInfo: tkInfo,
			Err:    errStr,
		})
	})

	type verifyNotaryStruct struct {
		EvName    string `json:"event"`
		TxHash    string `json:"hash"`
		FilePaths string `json:"filePaths"`
	}
	window.On(&gotron.Event{Event: "verifyNotary"}, func(bin []byte) {
		var data verifyNotaryStruct
		json.Unmarshal(bin, &data)

		var errStr string
		verified, err := controller.CheckNotary(data.TxHash, data.FilePaths)
		if err != nil {
			errStr = err.Error()
		}

		type notaryEv struct {
			*gotron.Event
			Verified bool   `json:"verified"`
			Err      string `json:"err"`
		}
		window.Send(&notaryEv{
			Event:    &gotron.Event{Event: "notary"},
			Verified: verified,
			Err:      errStr,
		})
	})

	type getVotingInfoStruct struct {
		EvName string `json:"event"`
		VtHash string `json:"vtHash"`
	}
	window.On(&gotron.Event{Event: "getVotingInfo"}, func(bin []byte) {
		var data getVotingInfoStruct
		json.Unmarshal(bin, &data)

		var errStr string
		vtInfo, err := controller.GetVotingInfo(data.VtHash)
		if err != nil {
			errStr = err.Error()
		}

		vtResult := controller.GetVotingResult(data.VtHash)

		type votingInfoEv struct {
			*gotron.Event
			VtHash   string                `json:"vtHash"`
			VtInfo   protocol.VotingInfo   `json:"vtInfo"`
			VtResult protocol.VotingResult `json:"vtResult"`
			Err      string                `json:"err"`
		}
		window.Send(&votingInfoEv{
			Event:    &gotron.Event{Event: "vtInfo"},
			VtHash:   data.VtHash,
			VtInfo:   vtInfo,
			VtResult: vtResult,
			Err:      errStr,
		})
	})

	window.On(&gotron.Event{Event: "getMempool"}, func(bin []byte) {
		transactions := controller.GetMempool()

		type mempoolEv struct {
			*gotron.Event
			Transactions []protocol.TransactionInt `json:"transactions"`
		}
		window.Send(&mempoolEv{
			Event:        &gotron.Event{Event: "mempool"},
			Transactions: transactions,
		})
	})

	window.On(&gotron.Event{Event: "startForging"}, func(bin []byte) {
		var errStr string
		if err := controller.StartForging(); err != nil {
			errStr = err.Error()
		}

		type forgingEv struct {
			*gotron.Event
			Err string `json:"err"`
		}
		window.Send(&forgingEv{
			Event: &gotron.Event{Event: "forging"},
			Err:   errStr,
		})

		go forgeTimeLoop(window)
	})

	window.On(&gotron.Event{Event: "stopForging"}, func(bin []byte) {
		controller.StopForging()
	})

	window.On(&gotron.Event{Event: "getForgingState"}, func(bin []byte) {
		state := controller.GetForgingState()

		type forgingEv struct {
			*gotron.Event
			State bool `json:"state"`
		}
		window.Send(&forgingEv{
			Event: &gotron.Event{Event: "forgingstate"},
			State: state,
		})
	})

	window.On(&gotron.Event{Event: "getTransactions"}, func(bin []byte) {
		transactions := controller.GetAllTransactions()

		type transactionsEv struct {
			*gotron.Event
			Transactions []protocol.TransactionInt `json:"transactions"`
		}
		window.Send(&transactionsEv{
			Event:        &gotron.Event{Event: "transactions"},
			Transactions: transactions,
		})
	})

	window.On(&gotron.Event{Event: "getPeers"}, func(bin []byte) {
		peers := controller.GetPeers()

		type peersEv struct {
			*gotron.Event
			Peers []map[string]string `json:"peers"`
		}
		window.Send(&peersEv{
			Event: &gotron.Event{Event: "peers"},
			Peers: peers,
		})
	})

	type addPeerStruct struct {
		EvName string `json:"event"`
		IP     string `json:"ip"`
	}
	window.On(&gotron.Event{Event: "addPeer"}, func(bin []byte) {
		var data addPeerStruct
		json.Unmarshal(bin, &data)

		var errStr string
		if err := controller.Connect(data.IP); err != nil {
			errStr = err.Error()
		}

		type addPeerEv struct {
			*gotron.Event
			Err string `json:"err"`
		}
		window.Send(&addPeerEv{
			Event: &gotron.Event{Event: "addpeer"},
			Err:   errStr,
		})
	})

	// Open dev tools must be used after window.Start
	// window.OpenDevTools()

	// Wait for the application to close
	<-done
}

type forgeTimeEv struct {
	*gotron.Event
	ForgeTime int `json:"ftime"`
}

func forgeTimeLoop(window *gotron.BrowserWindow) {
	if !controller.GetForgingState() {
		return
	}

	window.Send(&forgeTimeEv{
		Event:     &gotron.Event{Event: "ftime"},
		ForgeTime: controller.GetForgeTime(),
	})

	timeout := time.NewTimer(time.Duration(2) * time.Second)
	<-timeout.C

	forgeTimeLoop(window)
}
