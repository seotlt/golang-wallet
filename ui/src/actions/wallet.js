export const CHANGE_WALLET = 'CHANGE_WALLET';

export default function changeWallet(newWallet) {
  return {
    type: CHANGE_WALLET,
    payload: newWallet
  };
}
