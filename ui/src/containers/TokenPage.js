import React, { Component } from 'react';
import { connect } from 'react-redux';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CreateToken from '../components/CreateToken';
import TransferToken from '../components/TransferToken';
import SupplyToken from '../components/SupplyToken';
import TokenInfo from '../components/TokenInfo';

class TokenPage extends Component {
  state = {
    selectedTab: 0,
  }

  handleChange = (event, value) => {
    this.setState({ selectedTab: value });
  };

  setTab = selectedTab => {
    const { currentWallet } = this.props;

    switch (selectedTab) {
      case 0:
        return ( <CreateToken /> );
      case 1:
        return ( <TransferToken currentWallet={currentWallet} /> );
      case 2:
        return ( <SupplyToken currentWallet={currentWallet} /> );
    }

    return '';
  }

  render() {
    const { selectedTab } = this.state;
    return (
      <div>
        <Tabs
          value={selectedTab}
          onChange={this.handleChange}
          centered
        >
          <Tab label="Create" />
          <Tab label="Transfer" />
          <Tab label="Supply" />
        </Tabs>
        { this.setTab(selectedTab) }

        <TokenInfo />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentWallet: state.wallet
  };
}

export default connect(mapStateToProps)(TokenPage);
