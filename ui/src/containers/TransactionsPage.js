import React, { Component } from 'react';
import { connect } from 'react-redux';
import SendTX from '../components/SendTX';
import TransactionsHistory from '../components/TransactionsHistory';

class TransactionsPage extends Component {
  render() {
    const { currentWallet } = this.props;

    return (
      <div>
        <SendTX />
        {/* <TransactionsHistory currentWallet={currentWallet} /> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentWallet: state.wallet
  };
}

export default connect(mapStateToProps)(TransactionsPage);
