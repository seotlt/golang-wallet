import React, { Component } from 'react';
import CreateNotary from '../components/CreateNotary';
import VerifyNotary from '../components/VerifyNotary';

class NotaryPage extends Component {
  render() {
    return (
      <div>
        <CreateNotary />
        <VerifyNotary />
      </div>
    );
  }
}

export default NotaryPage;
