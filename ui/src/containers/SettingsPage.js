import React, { Component } from 'react';

import PeersList from '../components/PeersList';
import NodeSettings from '../components/NodeSettings';

class SettingsPage extends Component {
  render() {
    return (
      <div>
        <PeersList />
        <NodeSettings />
      </div>
    );
  }
}

export default SettingsPage;
