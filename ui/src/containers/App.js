// @flow
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AppNavBar from '../components/AppBar';
import changeWallet from '../actions/wallet';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  }
});

class App extends React.Component {
  render() {
    const {
      classes,
      children,
      currentWallet,
      changeWallet: changeWalletAction
    } = this.props;

    return (
      <div className={classes.root}>
        <AppNavBar
          currentWallet={currentWallet}
          changeWallet={changeWalletAction}
        />

        <main className={classes.content}>
          <div className={classes.toolbar} />

          {children}
        </main>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentWallet: state.wallet
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeWallet }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(App));
