import React, { Component } from 'react';

import ForgingSettings from '../components/ForgingSettings';
import Mempool from '../components/Mempool';

class ForgingPage extends Component {
  render() {
    return (
      <div>
        <ForgingSettings />
        <Mempool />
      </div>
    );
  }
}

export default ForgingPage;
