import React, { Component } from 'react';
import { connect } from 'react-redux';
import CreateVoting from '../components/CreateVoting';
import VotingInfo from '../components/VotingInfo';

class VotingPage extends Component {
  render() {
    const { currentWallet } = this.props;

    return (
      <div>
        <CreateVoting />
        <VotingInfo currentWallet={currentWallet}/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentWallet: state.wallet
  };
}

export default connect(mapStateToProps)(VotingPage);
