import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import HomePage from './containers/HomePage';
import TransactionsPage from './containers/TransactionsPage';
import ForgingPage from './containers/ForgingPage';
import SettingsPage from './containers/SettingsPage';
import TokenPage from './containers/TokenPage';
import VotingPage from './containers/VotingPage';
import Slide from '@material-ui/core/Slide';
import { SnackbarProvider } from 'notistack';
import NotaryPage from './containers/NotaryPage';

export default () => (
  <SnackbarProvider
    maxSnack={3}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    TransitionComponent={TransitionLeft}
    preventDuplicate
  >
    <App>
      <Switch>
        <Route path={routes.SETTINGS} component={SettingsPage} />
        <Route path={routes.FORGING} component={ForgingPage} />
        <Route path={routes.TRANSACTIONS} component={TransactionsPage} />
        <Route path={routes.TOKEN} component={TokenPage} />
        <Route path={routes.VOTING} component={VotingPage} />
        <Route path={routes.NOTARY} component={NotaryPage} />
        <Route path={routes.HOME} component={HomePage} />
      </Switch>
    </App>
  </SnackbarProvider>
);

function TransitionLeft(props) {
  return <Slide {...props} direction="left" />;
}