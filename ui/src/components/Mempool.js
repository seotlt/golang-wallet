import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import UpdateIcon from '@material-ui/icons/Autorenew';

import NestedList from './NestedList';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class Mempool extends Component {
  state = {
    transactions: []
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");
    this.ws.onopen = event => {
      this.ws.send(JSON.stringify({
        "event": "getMempool"
      }));
    }

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);

      switch(obj.event) {
        case 'mempool':
          this.setState({ transactions: obj.transactions || []});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleClickUpdate = () => {
    this.ws.send(JSON.stringify({
      "event": "getMempool"
    }));
  };

  render() {
    const { classes } = this.props;
    const { transactions } = this.state;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <div>
              <Typography
                className={classes.title}
                color="textPrimary"
                inline
                gutterBottom
              >
                Mempool
              </Typography>

              <IconButton onClick={this.handleClickUpdate} aria-label="Update">
                <UpdateIcon />
              </IconButton>
            </div>

            { !transactions.length ? 
              <Typography
                color="textPrimary"
                align="center"
              >
                Mempool is empty
              </Typography> : 
                <NestedList items={transactions} />
            }
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(Mempool);
