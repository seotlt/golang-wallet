import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  list: {
    wordBreak: 'break-all'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class NestedList extends React.Component {
  render() {
    const { classes, items } = this.props;

    return (
      <div>
        {items.map(item => (
          <ExpansionPanel key={item.hash}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>{item.hash}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <List component="div" className={classes.list} disablePadding>
                {Object.keys(item).map(field => (
                  <ListItem button key={field}>
                    <ListItemText
                      primary={field}
                      secondary={JSON.stringify(item[field])}
                    />
                  </ListItem>
                ))}
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </div>
    );
  }
}

NestedList.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired,
  items: PropTypes.arrayOf(PropTypes.object)
};

export default withStyles(styles, { withTheme: true })(NestedList);
