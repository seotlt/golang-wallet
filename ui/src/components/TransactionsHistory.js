import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import UpdateIcon from '@material-ui/icons/Autorenew';

import NestedList from './NestedList';

const styles = theme => ({
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class TransactionsHistory extends Component {
  state = {
    transactions: []
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");
    this.ws.onopen = event => {
      this.ws.send(JSON.stringify({
        "event": "getTransactions"
      }));
    }
  
    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
  
      switch(obj.event) {
        case 'transactions':
          this.setState({ transactions: obj.transactions});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleClickUpdate = () => {
    this.ws.send(JSON.stringify({
      "event": "getTransactions"
    }));
  };

  render() {
    const { classes } = this.props;
    const { transactions } = this.state;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <div>
              <Typography
                className={classes.title}
                color="textPrimary"
                component="span"
                gutterBottom
                inline
              >
                Transactions history
              </Typography>

              <IconButton onClick={this.handleClickUpdate} aria-label="Update">
                <UpdateIcon />
              </IconButton>
            </div>

            <NestedList items={transactions} />
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(TransactionsHistory);
