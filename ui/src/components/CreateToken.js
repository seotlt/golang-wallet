import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  },
  formControl: {
    minWidth: 120,
  },
});

class CreateToken extends Component {
  state = {
    tkName: '',
    isMintable: false,
    longUnit: '',
    shortUnit: '',
    supply: 0,
    realPart: 0,
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          enqueueSnackbar('Transaction has been sent!', {variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleChangeCheckbox = name => event => {
    this.setState({ [name]: event.target.checked });
  }

  handleClickCreate = () => {
    const { supply, realPart, isMintable } = this.state;
    let { tkName, longUnit, shortUnit } = this.state;

    tkName = tkName.trim();
    longUnit = longUnit.trim();
    shortUnit = shortUnit.trim();

    try {
      if (tkName === '') {
        throw new Error('Invalid token name: Can\'t be empty!');
      }

      if (tkName.length < 2) {
        throw new Error('Invalid token name: Must be greater than 2!');
      }

      if (longUnit === '') {
        throw new Error('Invalid long unit name: Can\'t be empty!');
      }
      if (longUnit.length > 100) {
        throw new Error('Invalid long unit name: Too long! Maximum 100 symbols!');
      }

      if (shortUnit === '') {
        throw new Error('Invalid short unit name: Can\'t be empty!');
      }
      if (shortUnit.length > 3) {
        throw new Error('Invalid short unit name: Too long! Maximum 3 symbols!');
      }

      if (supply <= 0) {
        throw new Error('Invalid initial supply: must be greater than 0!');
      }

      const supplyStr = supply + '';
      const supplyParts = supplyStr.split('.')
      if (supplyParts[1]) {
        if (supplyParts[1].length > realPart) {
          throw new Error('Invalid initial supply: Length of decimal part of supply doesn\'t match entered minimal unit!');
        }
      }

      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 2,
        "params": {
          tkName,
          supply,
          action: '0',
          unitLong: longUnit,
          unitShort: shortUnit,
          decimal: String(realPart),
          mintable: isMintable ? '1' : '0',
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { tkName, isMintable, longUnit, shortUnit, supply, realPart } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Create Token
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <TextField
                  value={tkName}
                  onChange={this.handleChangeInput('tkName')}
                  label="Token Name"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <TextField
                  value={longUnit}
                  onChange={this.handleChangeInput('longUnit')}
                  label="Long Unit Name"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <TextField
                  value={shortUnit}
                  onChange={this.handleChangeInput('shortUnit')}
                  label="Short Unit Name (3 symbols)"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  value={supply}
                  type="number"
                  inputProps={{ min: '0' }}
                  onChange={this.handleChangeInput('supply')}
                  label="Initial Supply"
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={3}>
                <FormControl className={classes.formControl} fullWidth>
                  <InputLabel htmlFor="minimal-unit">Minimal Unit</InputLabel>
                  <Select
                    value={realPart}
                    onChange={this.handleChangeInput('realPart')}
                    inputProps={{
                      name: 'minimal-unit',
                      id: 'minimal-unit',
                    }}
                  >
                    <MenuItem value={0}>1</MenuItem>
                    <MenuItem value={1}>0.1</MenuItem>
                    <MenuItem value={2}>0.01</MenuItem>
                    <MenuItem value={3}>0.001</MenuItem>
                    <MenuItem value={4}>0.0001</MenuItem>
                    <MenuItem value={5}>0.00001</MenuItem>
                    <MenuItem value={6}>1e-6</MenuItem>
                    <MenuItem value={7}>1e-7</MenuItem>
                    <MenuItem value={8}>1e-8</MenuItem>
                    <MenuItem value={9}>1e-9</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} sm={3}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={isMintable}
                      onChange={this.handleChangeCheckbox('isMintable')}
                    />
                  }
                  label="Mintable"
                  labelPlacement="start"
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickCreate}
                  fullWidth
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(CreateToken));