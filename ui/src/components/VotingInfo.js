import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  },
  formControl: {
    minWidth: 120,
  },
});

let VotingHash = '';

class VotingInfo extends Component {
  state = {
    votingHash: '',
    vtInfo: {},
    other: '',
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'vtInfo':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          VotingHash = obj.vtHash;
          const votingInfo = {
            from: obj.vtInfo.From,
            question: obj.vtInfo.Question,
            closed: obj.vtInfo.Closed,
            yes: obj.vtResult.Yes,
            no: obj.vtResult.No,
            abstain: obj.vtResult.Abstain,
            other: obj.vtResult.Other,
          };

          this.setState({ vtInfo: {info: votingInfo, voters: obj.vtResult.Voters} });
        break;
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          enqueueSnackbar('Transaction has been sent!', {variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  alreadyVoted = () => {
    const { currentWallet } = this.props;
    const { vtInfo } = this.state;
    if (vtInfo.voters[currentWallet]) {
      return true;
    }

    return false;
  }

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClickGet = () => {
    const { votingHash } = this.state;
    try {
      this.ws.send(JSON.stringify({
        "event": "getVotingInfo",
        "vtHash": votingHash,
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  handleClickAnswer = answer => {
    try {
      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 3,
        "params": {
          questHash: VotingHash,
          answer,
          action: '1',
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  handleClickClose = () => {
    try {
      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 3,
        "params": {
          questHash: VotingHash,
          action: '2',
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes, currentWallet } = this.props;
    const { votingHash, vtInfo, other } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Get Voting Info
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <TextField
                  value={votingHash}
                  onChange={this.handleChangeInput('votingHash')}
                  label="Hash Of Voting Transaction"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickGet}
                  fullWidth
                >
                  Get
                </Button>
              </Grid>

              {vtInfo.info ?
                (
                  <React.Fragment>
                    <Grid item xs={12}>
                      <Table>
                        <TableBody>
                          {
                            Object.keys(vtInfo.info).map(field => (
                              <TableRow key={field}>
                                <TableCell component="th" scope="row">
                                  {field}
                                </TableCell>
                                <TableCell align="right">
                                  {JSON.stringify(vtInfo.info[field])}
                                </TableCell>
                              </TableRow>
                            ))
                          }
                        </TableBody>
                      </Table>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <Button
                        variant="contained"
                        color="primary"
                        variant={vtInfo.voters[currentWallet] === '0' ? 'outlined' : 'contained'}
                        disabled={this.alreadyVoted()}
                        onClick={() => this.handleClickAnswer('0')}
                        fullWidth
                      >
                        Yes
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <Button
                        variant="contained"
                        color="primary"
                        variant={vtInfo.voters[currentWallet] === '1' ? 'outlined' : 'contained'}
                        disabled={this.alreadyVoted()}
                        onClick={() => this.handleClickAnswer('1')}
                        fullWidth
                      >
                        No
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <Button
                        variant="contained"
                        color="primary"
                        variant={vtInfo.voters[currentWallet] === '2' ? 'outlined' : 'contained'}
                        disabled={this.alreadyVoted()}
                        onClick={() => this.handleClickAnswer('2')}
                        fullWidth
                      >
                        Abstain
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                      <TextField
                        value={other}
                        disabled={this.alreadyVoted()}
                        onChange={this.handleChangeInput('other')}
                        label="Other"
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <Button
                        variant="contained"
                        color="primary"
                        variant={vtInfo.voters[currentWallet] < '0' || vtInfo.voters[currentWallet] > '2' ? 'outlined' : 'contained'}
                        disabled={this.alreadyVoted()}
                        onClick={() => this.handleClickAnswer(other)}
                        fullWidth
                      >
                        Answer
                      </Button>
                    </Grid>
                    {currentWallet === vtInfo.info.from ? 
                      <Grid item xs={12}>
                        <Button
                          variant="contained"
                          color="secondary"
                          disabled={vtInfo.info.closed}
                          onClick={this.handleClickClose}
                          fullWidth
                        >
                          Close Voting
                        </Button>
                      </Grid>
                      : ''
                    }
                  </React.Fragment>
                ) : ''
              }
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(VotingInfo));