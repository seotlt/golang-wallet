import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  errText: {
    color: '#ff0000',
    fontSize: 17,
    textAlign: 'center'
  },
  resText: {
    color: '#00ff00',
    fontSize: 17,
    textAlign: 'center'
  }
});

class SendCoin extends Component {
  state = {
    recipientAddress: '',
    coinsAmount: 0,
    txtype: 1,
    result: '',
    err: ''
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);

      switch(obj.event) {
        case 'sendtx':
          this.setState({ err: obj.err, result: obj.result });
        break;
      }
    }
  }

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleChangeType = event => {
    this.setState({ txtype: event.target.value });
  }

  handleChangeRecipient = event => {
    this.setState({ recipientAddress: event.target.value });
  };

  handleChangeCoinsAmount = event => {
    this.setState({ coinsAmount: event.target.value });
  };

  handleClickSend = () => {
    const { coinsAmount, txtype } = this.state;
    let { recipientAddress } = this.state;

    try {
      if (txtype < 1 || txtype > 2) {
        throw Error('Type of the sended transaction must be 1 or 2!');
      }

      if (recipientAddress.length === 40) {
        recipientAddress = `0x${recipientAddress}`;
      }

      if (recipientAddress.length !== 42) {
        throw Error('Invalid recipient address!');
      }

      if (coinsAmount < 0) {
        throw Error('Amount of sending coins cannot be less than zero!');
      }

      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": txtype,
        "to": recipientAddress,
        "amount": coinsAmount
      }));
    } catch (err) {
      this.setState({ err: err.message });
    }
  };

  render() {
    const { classes } = this.props;
    const { err, txtype, result, recipientAddress, coinsAmount } = this.state;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Typography
              className={classes.title}
              color="textPrimary"
              gutterBottom
            >
              Send transaction
            </Typography>

            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography className={classes.errText}>{err}</Typography>
                <Typography className={classes.resText}>{result}</Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <InputLabel htmlFor="txtype">Type</InputLabel>
                <Select
                  className={classes.textField}
                  value={txtype}
                  onChange={this.handleChangeType}
                  inputProps={{
                    name: 'txtype',
                    id: 'txtype'
                  }}
                  fullWidth
                >
                  <MenuItem value={1}>Coin</MenuItem>
                  <MenuItem value={2}>State</MenuItem>
                </Select>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  className={classes.textField}
                  type="number"
                  value={coinsAmount}
                  inputProps={{ min: '0' }}
                  label="Amount of coins"
                  onChange={this.handleChangeCoinsAmount}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className={classes.textField}
                  type="text"
                  value={recipientAddress}
                  label="Recipient address"
                  onChange={this.handleChangeRecipient}
                  fullWidth
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button size="large" onClick={this.handleClickSend} fullWidth>
              Send
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(SendCoin);
