import React, { Component } from 'react';
import { withStyles, withTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

import UpdateIcon from '@material-ui/icons/Autorenew';
import PeerOnIcon from '@material-ui/icons/SignalCellular4Bar';
import PeerOffIcon from '@material-ui/icons/SignalCellularOff';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  }
});

class NodeSettings extends Component {
  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;
      switch(obj.event) {
        case 'rpc':
          enqueueSnackbar(`RPC started on port ${obj.port}!`, {autoHideDuration: 15000, variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close()
  };

  handleClickStart = () => {
    this.ws.send(JSON.stringify({
      "event": "startRPC",
    }));
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Node Settings
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography>
                  RPC:
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickStart}
                  fullWidth
                >
                  Start
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(NodeSettings));
