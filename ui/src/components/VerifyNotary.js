import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { withSnackbar } from 'notistack';
import Input from '@material-ui/core/Input';

const styles = theme => ({
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  input: {
    display: 'none'
  },
  res: {
    color: '#00ff00'
  },
  chip: {
    margin: theme.spacing.unit * 0.5,
  },
  formControl: {
    minWidth: 120,
  },
});

class VerifyNotary extends Component {
  state = {
    txhash: '',
    filepaths: '',
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'notary':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }

          if (obj.verified) {
            enqueueSnackbar(`Notary successfuly verified!`, { autoHideDuration: 15000, variant: 'success'});
          } else {
            enqueueSnackbar(`Notary verification failed!`, { autoHideDuration: 15000, variant: 'warning'});
          }
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClickDelete = fileDelete => () => {
    const { filepaths } = this.state;
    this.setState({ filepaths: filepaths.filter(filepath => filepath !== fileDelete)});
  };

  handleChangeFile = event => {
    const filePath = event.target.files[0].path;
      
    this.setState({ filepaths: filePath });
  }

  handleClickVerify = () => {
    const { filepaths, txhash } = this.state;

    try {
      if (!filepaths.length) {
        throw new Error('Need one file or folder uploaded!');
      }

      this.ws.send(JSON.stringify({
        "event": "verifyNotary",
        "hash": txhash,
        "filePaths": filepaths,
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { filepaths, txhash } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Verify Notary
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={txhash}
                  onChange={this.handleChangeInput('txhash')}
                  label="Hash Of Notary Transaction"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Typography>
                  {`File Path: ${filepaths}`}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Input
                  type="file"
                  id="upload-folder"
                  onChange={this.handleChangeFile}
                  className={classes.input}
                  inputProps={{ webkitdirectory: "true" }}
                />
                <label htmlFor="upload-folder">
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    fullWidth
                  >
                    Upload Folder
                  </Button>
                </label>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Input
                  type="file"
                  id="upload-filepath"
                  onChange={this.handleChangeFile}
                  className={classes.input}
                />
                <label htmlFor="upload-filepath">
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    fullWidth
                  >
                    Upload File
                  </Button>
                </label>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickVerify}
                  fullWidth
                >
                  Verify
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(VerifyNotary));