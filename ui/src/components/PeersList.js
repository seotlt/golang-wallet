import React, { Component } from 'react';
import { withStyles, withTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

import UpdateIcon from '@material-ui/icons/Autorenew';
import PeerOnIcon from '@material-ui/icons/SignalCellular4Bar';
import PeerOffIcon from '@material-ui/icons/SignalCellularOff';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  }
});

class PeersList extends Component {
  state = {
    peers: [],
    ipAddress: '',
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");
    this.ws.onopen = event => {
      this.ws.send(JSON.stringify({
        "event": "getPeers"
      }));
    }

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;
      switch(obj.event) {
        case 'peers':
          console.log(obj.peers);
          this.setState({ peers: obj.peers || []});
        break;
        case 'addpeer':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            return;
          }

          enqueueSnackbar('Peer has been added!', {variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close()
  };

  handleClickUpdate = () => {
    this.ws.send(JSON.stringify({
      "event": "getPeers"
    }));
  };

  handleClickAdd = () => {
    const { ipAddress } = this.state
    this.ws.send(JSON.stringify({
      "event": "addPeer",
      "ip": ipAddress
    }));
  }

  handleChangeIP = event => {
    this.setState({ ipAddress: event.target.value });
  };

  render() {
    const { classes } = this.props;
    const { peers, ipAddress, err } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Peers List
                </Typography>

                <IconButton
                  onClick={this.handleClickUpdate}
                  aria-label="Update"
                >
                  <UpdateIcon />
                </IconButton>
              </Grid>
              <Grid item xs={12}>
                <Typography
                  className={classes.err}
                  align="center"
                >
                  {err}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  value={ipAddress}
                  onChange={this.handleChangeIP}
                  placeholder="IP Address"
                  fullWidth
                />
              </Grid>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickAdd}
                  fullWidth
                >
                  Add
                </Button>
              </Grid>
              <Grid item xs={12}>
                <List>
                  {peers.map((peer, i) => {
                    return (
                      <ListItem key={peer.ip}>
                        <ListItemIcon>
                          {peer.status === "1" ? <PeerOnIcon /> : <PeerOffIcon />}
                        </ListItemIcon>

                        <ListItemText primary={peer.ip} />
                      </ListItem>
                    );
                  })}
                </List>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(PeersList));
