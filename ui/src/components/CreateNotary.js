import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import { withSnackbar } from 'notistack';
import Input from '@material-ui/core/Input';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  input: {
    display: 'none'
  },
  res: {
    color: '#00ff00'
  },
  chip: {
    margin: theme.spacing.unit * 0.5,
  },
  formControl: {
    minWidth: 120,
  },
});

class CreateNotary extends Component {
  state = {
    filepaths: [],
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }

          enqueueSnackbar(`Notary has been created!\nHash: ${obj.hash}.\nSave it to verify your rights!`, { autoHideDuration: 15000, variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClickDelete = fileDelete => () => {
    const { filepaths } = this.state;
    this.setState({ filepaths: filepaths.filter(filepath => filepath !== fileDelete)});
  };

  handleChangeFile = event => {
    const files = event.target.files;
    const { filepaths } = this.state;
    for (let i = 0; i < files.length; i++) {
      if (filepaths.includes(files[i].path)) {
        return;
      }
      filepaths.push(files[i].path);
    }
      
    this.setState({ filepaths });
  }

  handleClickCreate = () => {
    const { filepaths } = this.state;

    try {
      if (!filepaths.length) {
        throw new Error('Need at least one file to uploaded!');
      }
      const resFiles = filepaths.join(' ');
      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 4,
        "params": {
          filePaths: resFiles,
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { filepaths } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Create Notary
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography>
                  Files
                </Typography>
                {filepaths.map((filepath, index) => {
                  const label = filepath.split('/')
                  return (
                    <Chip
                      key={index}
                      label={label[label.length-1]}
                      color="primary"
                      onDelete={this.handleClickDelete(filepath)}
                      className={classes.chip}
                    />
                  );
                })}
              </Grid>
              <Grid item xs={12}>
                <Input
                  type="file"
                  id="upload-file"
                  onChange={this.handleChangeFile}
                  className={classes.input}
                  inputProps={{ multiple: true }}
                />
                <label htmlFor="upload-file">
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    fullWidth
                  >
                    Upload
                  </Button>
                </label>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickCreate}
                  fullWidth
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(CreateNotary));