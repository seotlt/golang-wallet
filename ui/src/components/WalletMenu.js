import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';

import Button from '@material-ui/core/Button';
import ArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import DeleteIcon from '@material-ui/icons/Clear';
import CopyIcon from '@material-ui/icons/FileCopy'
import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import KeyIcon from '@material-ui/icons/VpnKey';
import CircularProgress from '@material-ui/core/CircularProgress';
import copy from 'copy-to-clipboard';
import Tooltip from '@material-ui/core/Tooltip';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { ITEM_HEIGHT } from '../constants/app';

const styles = theme => ({
  progress: {
    marginRight: theme.spacing.unit * 10,
    marginLeft: theme.spacing.unit * 10,
  },
  importantTxt: {
    color: '#ff0000'
  }
});

const delIcon = {
  position: 'relative',
  padding: '11px 0',
  minWidth: '45px'
};

const walletView = {
  width: '355px',
  display: 'inline-block',
  borderRight: '2px solid #000',
  margin: '5px 0'
};

class WalletMenu extends React.Component {
  state = {
    anchorEl: null,
    balance: '',
    balanceType: 0,
    dialogOpen: false,
    dialogDelOpen: false,
    dialogWalletOpen: false,
    selectedWallet: {},
    walletPassword: '',
    showWalletPassword: false,
    password: '',
    confirmPassword: '',
    showPassword: false,
    showConfirmPassword: false,
    showPrivateKey: false,
    privateKey: '',
    nameFileDel: null,
    nameFileCopy: null,
    wallets: null,
    walletLoading: false
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");
    
    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;
      switch(obj.event) {
        case 'wallets':
          this.setState({ wallets: obj.wallets || [] });
          console.log(obj.wallets);
        break;
        case 'importWall':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            return;
          }

          this.setState({
            dialogOpen: false,
            anchorEl: null,
            walletPassword: '',
            password: '',
            confirmPassword: '',
            privateKey: ''
          });
        break;
        case 'unlockWall':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            this.setState({ walletLoading: false });
            return;
          }
          
          const { changeWallet } = this.props;
          
          const address = '0x' + obj.address;
          changeWallet(address);
          
          this.ws.send(JSON.stringify({
            "event": "getBalance",
            "type": 0,
            "address": address 
          }));
          this.setState({
            dialogWalletOpen: false,
            anchorEl: null,
            walletLoading: false,
            balanceType: 0,
            walletPassword: '',
            password: '',
            confirmPassword: '',
            privateKey: '',
          });

          enqueueSnackbar(`Wallet ${address} has been selected!`, {variant: 'success'});
        break;
        case 'balance':
          this.setState({ balance: obj.balance + '' });
        break;
        case 'privateKey':
          copy(obj.privateKey);
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleClick = event => {
    this.ws.send(JSON.stringify({
      "event": "getWallets",
    }));
    this.setState({ anchorEl: event.currentTarget, wallets: null });
  };

  handleSelect = event => {
    const address = event.target.getAttribute('value');
    const fileName = event.target.getAttribute('filename');

    this.setState({
      selectedWallet: {
        address,
        fileName
      },
      dialogWalletOpen: true
    });
  };

  handleClickRefresh = () => {
    const { currentWallet } = this.props;
    const { balanceType } = this.state;
    this.ws.send(JSON.stringify({
      "event": "getBalance",
      "type": balanceType,
      "address": currentWallet
    }));
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleCloseDialog = () => {
    this.setState({ dialogOpen: false });
  };

  handleDeleteClose = () => {
    this.setState({ dialogDelOpen: false });
  };

  handleClosePasswordDialog = () => {
    this.setState({ dialogWalletOpen: false });
  };

  handleChangeWalletPassword = event => {
    this.setState({ walletPassword: event.target.value });
  };

  handleClickShowWalletPassword = () => {
    this.setState(state => ({ showWalletPassword: !state.showWalletPassword }));
  };

  handleClickSubmitPass = () => {
    const { selectedWallet, walletPassword } = this.state;
    
    this.ws.send(JSON.stringify({
      "event": "unlockWallet",
      "filename": selectedWallet.fileName,
      "password": walletPassword
    }));

    this.setState({ walletLoading: true });
  };

  handleClickAddNew = () => {
    this.setState({ dialogOpen: true });
  };

  handleClickDelete = nameFileDel => {
    this.setState({
      nameFileDel,
      dialogDelOpen: true
    });
  };

  handleClickCopy = nameFileCopy => {
    this.setState({
      nameFileCopy
    });
    copy('0x'+(nameFileCopy.split('.'))[0])
  }

  handleChangePrivateKey = event => {
    this.setState({ privateKey: event.target.value });
  };

  handleClickCreate = () => {
    const { password, confirmPassword } = this.state;
    let { privateKey } = this.state;
    try {
      if (password === '') {
        throw Error('Passwords are required!');
      }

      if (password !== confirmPassword) {
        throw Error('Passwords must be the same!');
      }

      if (privateKey !== '') {
        if (privateKey.length === 64) {
          privateKey = `0x${privateKey}`;
        }

        if (privateKey.length !== 66) {
          throw Error('Invalid private key!');
        }

        this.ws.send(JSON.stringify({
          "event": "importWallet",
          "privkey": privateKey,
          "password": password
        }));
      } else {
        this.ws.send(JSON.stringify({
          "event": "createWallet",
          "password": password
        }));

        this.setState({ dialogOpen: false, anchorEl: null });
      }
    } catch (err) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(err.message, {variant: 'error'});
    }
  };

  handleClickPrivateKey = () => {
    this.ws.send(JSON.stringify({
      "event": "getPrivateKey",
    }));
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleClickShowConfirmPassword = () => {
    this.setState(state => ({
      showConfirmPassword: !state.showConfirmPassword
    }));
  };

  handleClickShowPrivateKey = () => {
    this.setState(state => ({ showPrivateKey: !state.showPrivateKey }));
  };

  handleChangePassword = event => {
    this.setState({ password: event.target.value });
  };

  handleChangeConfirmPassword = event => {
    this.setState({ confirmPassword: event.target.value });
  };

  handleClickChangeBalance = () => {
    const { currentWallet } = this.props
    const newBalanceType = this.state.balanceType === 0 ? 1 : 0;
    this.setState({ balanceType: newBalanceType });

    this.ws.send(JSON.stringify({
      "event": "getBalance",
      "type": newBalanceType,
      "address": currentWallet,
    }));
  }

  deleteWallet = () => {
    const { nameFileDel } = this.state;
    this.ws.send(JSON.stringify({
      "event": "deleteWallet",
      "filename": nameFileDel
    }));

    this.setState({
      dialogDelOpen: false,
      anchorEl: null
    });
  };

  render() {
    const {
      balance,
      balanceType,
      anchorEl,
      dialogOpen,
      dialogDelOpen,
      dialogWalletOpen,
      showWalletPassword,
      showPassword,
      showConfirmPassword,
      showPrivateKey,
      wallets,
      walletLoading
    } = this.state;
    const { currentWallet, classes } = this.props;
    const open = Boolean(anchorEl);

    return (
      <div>
        <IconButton
          color="inherit"
          aria-label="Add wallet"
          onClick={this.handleClickAddNew}
        >
          <AddIcon />
        </IconButton>
        {currentWallet
          ? 
            <span>
              <IconButton
                color="inherit"
                aria-label="Refresh balance"
                onClick={this.handleClickRefresh}
              >
                <RefreshIcon />
              </IconButton>
              <IconButton
                color="inherit"
                aria-label="Get private key"
                onClick={this.handleClickPrivateKey}
              >
                <KeyIcon />
              </IconButton>
            </span>
          : ''
        }

        <Button
          color="inherit"
          aria-label="More"
          aria-owns={open ? 'long-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          {currentWallet
            ? `${currentWallet.substring(0, 20)}...`
            : 'Choose wallet'}
          <ArrowDownIcon />
        </Button>

        {currentWallet
          ? (
            <Button
              color="inherit"
              aria-label="Switch balance"
              aria-haspopup="true"
              onClick={this.handleClickChangeBalance}
            >
              {balanceType === 0 
                ? `Coins: ${balance}`
                : `States: ${balance}`
              }
            </Button>
          ) : ''}

        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: '500px'
            }
          }}
        >
          { wallets == null ? <CircularProgress className={classes.progress} /> : ''}
          { wallets != null && !wallets.length ? 
            <Typography
              color="textPrimary"
              align="center"
            >
              You have no wallets yet
            </Typography> : ''
          }
          { wallets != null && wallets.map(wallet => (
            <div key={wallet.filename}>
              <MenuItem
                style={walletView}
                value={`0x${wallet.address}`}
                selected={`0x${wallet.address}` === currentWallet}
                filename={wallet.filename}
                onClick={this.handleSelect}
              >
                0x{wallet.address}
              </MenuItem>

              
              <Button
                style={delIcon}
                onClick={() => {
                  this.handleClickCopy(wallet.filename)
                }}
              >
                <Tooltip title="Copy">
                <CopyIcon/>
                </Tooltip>
              </Button>

              <Button
                style={delIcon}
                onClick={() => {
                  this.handleClickDelete(wallet.filename);
                }}
              >
                <DeleteIcon />
              </Button>
            </div>
          ))}

          <Dialog
            open={dialogDelOpen}
            onClose={this.handleDeleteClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to delete the wallet?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleDeleteClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.deleteWallet} color="primary" autoFocus>
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </Menu>

        <Dialog
          open={dialogOpen}
          onClose={this.handleCloseDialog}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Create new wallet</DialogTitle>
          <DialogContent>
            <DialogContentText>
                To create a new wallet, please enter password here. <br />
              <Typography
                className={classes.importantTxt}
              >
                If you already have private key, please enter it in the field
                below.
              </Typography>
            </DialogContentText>
            <TextField
              onChange={this.handleChangePrivateKey}
              autoFocus
              margin="dense"
              id="privateKey"
              label="Private Key"
              type={showPrivateKey ? 'text' : 'password'}
              fullWidth
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this.handleClickShowPrivateKey}
                    >
                      {showPrivateKey ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />

            <TextField
              onChange={this.handleChangePassword}
              autoFocus
              margin="dense"
              id="password"
              label="Password"
              type={showPassword ? 'text' : 'password'}
              fullWidth
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this.handleClickShowPassword}
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />

            <TextField
              onChange={this.handleChangeConfirmPassword}
              margin="dense"
              id="passwordAgain"
              label="Confirm password"
              type={showConfirmPassword ? 'text' : 'password'}
              fullWidth
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this.handleClickShowConfirmPassword}
                    >
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleClickCreate} color="primary">
              Create
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={dialogWalletOpen}
          onClose={this.handleClosePasswordDialog}
          aria-labelledby="wallet-password-title"
        >
          <DialogTitle id="wallet-password-title">
            Enter password to unlock wallet
          </DialogTitle>
          <DialogContent>
            {
              walletLoading ? <CircularProgress className={classes.progress} /> 
              : 
              <div>
                <DialogContentText>
                  Please enter wallet password to unlock it.
                </DialogContentText>
                <TextField
                  onChange={this.handleChangeWalletPassword}
                  autoFocus
                  margin="dense"
                  id="walletPassword"
                  label="Password"
                  type={showWalletPassword ? 'text' : 'password'}
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={this.handleClickShowWalletPassword}
                        >
                          {showWalletPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </div>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClosePasswordDialog} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleClickSubmitPass} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles)(WalletMenu));
