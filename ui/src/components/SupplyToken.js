import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  },
  formControl: {
    minWidth: 120,
  },
});

class SupplyToken extends Component {
  state = {
    tkName: '',
    tokens: {},
    amount: 0,
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          enqueueSnackbar('Transaction has been sent!', {variant: 'success'});
        break;

        case 'userTokens':
          this.setState({ tokens: obj.tokens });
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClickTokenSelect = () => {
    const { currentWallet } = this.props;
    this.ws.send(JSON.stringify({
      "event": "getUserTokens",
      "address": currentWallet,
    }));
  }

  handleClickSupply = () => {
    const { amount } = this.state;
    let { tkName } = this.state;

    tkName = tkName.trim();

    try {
      if (tkName === '') {
        throw new Error('Invalid token name: Can\'t be empty!');
      }

      if (amount <= 0) {
        throw new Error('Invalid amount: must be greater than 0!');
      }

      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 2,
        "params": {
          tkName,
          amount,
          action: '2',
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { tkName, amount, tokens } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Supply Token
                </Typography>
              </Grid>
              <Grid item xs={12} sm={7}>
                <FormControl className={classes.formControl} fullWidth>
                  <InputLabel htmlFor="token-name">Token Name</InputLabel>
                  <Select
                    value={tkName}
                    onClick={this.handleClickTokenSelect}
                    onChange={this.handleChangeInput('tkName')}
                    inputProps={{
                      name: 'token-name',
                      id: 'token-name',
                    }}
                  >
                    {Object.keys(tokens).map( tkName => {
                      const balance = tokens[tkName];
                      return (
                        <MenuItem key={tkName} value={tkName}>{tkName}: {balance}</MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={5}>
                <TextField
                  value={amount}
                  type="number"
                  inputProps={{ min: '0' }}
                  onChange={this.handleChangeInput('amount')}
                  label="Amount"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickSupply}
                  fullWidth
                >
                  Supply
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(SupplyToken));