import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  errText: {
    color: '#ff0000',
    fontSize: 17,
    textAlign: 'center'
  },
  resText: {
    color: '#00ff00',
    fontSize: 17,
    textAlign: 'center'
  }
});

class SendTX extends Component {
  state = {
    recipientAddress: '',
    amount: 0,
    txtype: 0,
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          enqueueSnackbar('Transaction has been sent!', {variant: 'success'});
        break;
      }
    }
  }

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleChangeType = event => {
    this.setState({ txtype: event.target.value });
  }

  handleChangeRecipient = event => {
    this.setState({ recipientAddress: event.target.value });
  };

  handleChangeAmount = event => {
    this.setState({ amount: event.target.value });
  };

  handleClickSend = () => {
    const { amount, txtype } = this.state;
    let { recipientAddress } = this.state;

    try {
      if (txtype < 0 || txtype > 1) {
        throw Error('Invalid type of transaction!');
      }

      if (recipientAddress.length === 40) {
        recipientAddress = `0x${recipientAddress}`;
      }

      if (recipientAddress.length !== 42) {
        throw Error('Invalid recipient address!');
      }

      if (amount < 0) {
        throw Error('Amount of sending coins cannot be less than zero!');
      }

      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": txtype,
        "to": recipientAddress,
        "params": {
          amount
        },
      }));
    } catch (err) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(err.message, {variant: 'error'});
    }
  };

  render() {
    const { classes } = this.props;
    const { txtype, recipientAddress, amount } = this.state;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Typography
              className={classes.title}
              color="textPrimary"
              gutterBottom
            >
              Send transaction
            </Typography>

            <Grid container spacing={24}>
              <Grid item xs={12} sm={6}>
                <InputLabel htmlFor="txtype">Type</InputLabel>
                <Select
                  className={classes.textField}
                  value={txtype}
                  onChange={this.handleChangeType}
                  inputProps={{
                    name: 'txtype',
                    id: 'txtype'
                  }}
                  fullWidth
                >
                  <MenuItem value={0}>Coin</MenuItem>
                  <MenuItem value={1}>State</MenuItem>
                </Select>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  className={classes.textField}
                  type="number"
                  value={amount}
                  inputProps={{ min: '0' }}
                  label="Amount of coins"
                  onChange={this.handleChangeAmount}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className={classes.textField}
                  type="text"
                  value={recipientAddress}
                  label="Recipient address"
                  onChange={this.handleChangeRecipient}
                  fullWidth
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button size="large" onClick={this.handleClickSend} fullWidth>
              Send
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles)(SendTX));
