import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  },
  input: {
    display: 'none'
  },
  chip: {
    margin: theme.spacing.unit * 0.5,
  },
  formControl: {
    minWidth: 120,
  },
});

class CreateToken extends Component {
  state = {
    voters: [],
    question: '',
    address: '',
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'sendtx':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }

          enqueueSnackbar(`Voting has been created!\nHash: ${obj.hash}.\nSave it to share voters!`, { autoHideDuration: 15000, variant: 'success'});
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleChangeCheckbox = name => event => {
    this.setState({ [name]: event.target.checked });
  }
  
  handleClickDelete = voterDelete => () => {
    const { voters } = this.state;
    this.setState({ voters: voters.filter(voter => voter !== voterDelete)});
  };

  handleClickAdd = () => {
    const { voters, address } = this.state;
    if (voters.includes(address)) {
      return;
    }

    voters.push(address);
    this.setState({ voters, address: '' });
  }

  handleChangeFile = event => {
    const files = event.target.files;

    const reader = new FileReader();
    const self = this;
    reader.onload = function(){
      const content = reader.result;
      const fileVoters = content.split('\n');
      const { voters, address } = self.state;

      fileVoters.forEach(voter => {
        if (voters.includes(voter)) {
          return;
        }
  
        voters.push(voter);  
      });
      self.setState({ voters, address: '' });
    };
    reader.readAsText(files[0]);
  }

  handleClickCreate = () => {
    const { voters } = this.state;
    let { question } = this.state;

    question = question.trim();

    try {
      if (question === '') {
        throw new Error('Invalid question: Can\'t be empty!');
      }

      if (question.length > 500) {
        throw new Error('Invalid question: Too long! Maximum 500 symbols!');
      }

      const resVoters = voters.join(' ');
      this.ws.send(JSON.stringify({
        "event": "sendTX",
        "type": 3,
        "params": {
          question,
          voters: resVoters,
          action: '0',
        },
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { question, voters, address } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Create Voting
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={question}
                  onChange={this.handleChangeInput('question')}
                  label="Question"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Typography>
                  Voters
                </Typography>
                {voters.map((voter, index) => {
                  return (
                    <Chip
                      key={index}
                      label={voter}
                      color="primary"
                      onDelete={this.handleClickDelete(voter)}
                      className={classes.chip}
                    />
                  );
                })}
              </Grid>
              <Grid item xs={12} sm={8}>
                <TextField
                  value={address}
                  onChange={this.handleChangeInput('address')}
                  label="Address"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={2}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickAdd}
                  fullWidth
                >
                  Add
                </Button>
              </Grid>
              <Grid item xs={12} sm={2}>
                <Input
                  type="file"
                  id="upload-file"
                  onChange={this.handleChangeFile}
                  className={classes.input}
                  />
                <label htmlFor="upload-file">
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    fullWidth
                  >
                    Upload
                  </Button>
                </label>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickCreate}
                  fullWidth
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(CreateToken));