import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import UpdateIcon from '@material-ui/icons/Autorenew';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  container: {
    marginBottom: theme.spacing.unit
  },
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  }
});

let reduceTime; 
class ForgingSettings extends Component {
  state = {
    forgeState: false,
    forgeTime: 0,
    dispForgeTime: 0,
  }

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onopen = () => {
      this.ws.send(JSON.stringify({
        "event": "getForgingState"
      }));
    }
    
    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;
      switch(obj.event) {
        case 'forging':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          this.setState({ forgeState: true });
        break;
        case 'forgingstate':
          this.setState({ forgeState: obj.state });
        break;
        case 'ftime':
          if (this.state.forgeTime === obj.ftime) {
            break;
          }

          this.setState({ forgeTime: obj.ftime, dispForgeTime: obj.ftime });
          if (reduceTime) {
            clearInterval(reduceTime);
          }

          reduceTime = setInterval(() => {
            this.setState((prevState) => { 
              return {dispForgeTime: prevState.dispForgeTime ? prevState.dispForgeTime - 1 : 0} 
            })
          }, 1000);
        break;
      }
    }
  }

  componentWillUnmount = () => {
    this.ws.close();
  }

  handleClickStart = () => {
    this.ws.send(JSON.stringify({
      "event": "startForging"
    }));
  };

  handleClickStop = () => {
    this.ws.send(JSON.stringify({
      "event": "stopForging"
    }));

    this.setState({ state: false });
  };

  handleClickUpdate = () => {
    this.ws.send(JSON.stringify({
      "event": "getForgingState"
    }));
  }

  render() {
    const { classes } = this.props;
    const { forgeState, dispForgeTime } = this.state;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Typography
              className={classes.title}
              color="textPrimary"
              component="span"
              gutterBottom
              inline
            >
              Forging handle
            </Typography>

            <IconButton onClick={this.handleClickUpdate} aria-label="Update">
              <UpdateIcon />
            </IconButton>

            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  align="center"
                >
                  {forgeState ? 'Forging is running' : 'Forging is not running'}
                </Typography>
              </Grid>
              {forgeState ?
                <Grid item xs={12}>
                  <Typography
                    align="center"
                  >
                    Block will be forged in {dispForgeTime} seconds
                  </Typography>
                </Grid>
              : ''}
              <Grid item xs={12} sm={6}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickStart}
                  fullWidth
                >
                  Start
                </Button>
              </Grid>

              <Grid item xs={12} sm={6}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={this.handleClickStop}
                  fullWidth
                >
                  Stop
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles)(ForgingSettings));
