// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const styles = {
  container: {},
  card: {
    height: '75vh'
  }
};

class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent className={classes.card}>
            <iframe
              title="Block Explorer"
              src="http://178.250.156.169/blocks"
              width="100%"
              height="100%"
            />
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(Home);
