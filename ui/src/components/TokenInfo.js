import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import { withSnackbar } from 'notistack';

const styles = theme => ({
  title: {
    fontSize: 18
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  err: {
    color: '#ff0000'
  },
  res: {
    color: '#00ff00'
  },
  formControl: {
    minWidth: 120,
  },
});

let TokenName = '';

class TokenInfo extends Component {
  state = {
    tkName: '',
    address: '',
    tkInfo: {},
    tokenBalance: '',
  };

  componentDidMount = () => {
    this.ws = new WebSocket("ws://localhost:" + global.backendPort + "/web/app/events");

    this.ws.onmessage = (message) => {
      const obj = JSON.parse(message.data);
      const { enqueueSnackbar } = this.props;

      switch(obj.event) {
        case 'tkInfo':
          if (obj.err !== '') {
            enqueueSnackbar(obj.err, {variant: 'error'});
            break;
          }
          TokenName = obj.tkName;
          this.setState({ tkInfo: obj.tkInfo });
        break;
        case 'tokenbalance':
          const { tkInfo } = this.state;
          const balanceStr = obj.balance + '';
          const index = balanceStr.length - tkInfo.Decimal;
          const resBalance = `${balanceStr.substring(0, index)}.${balanceStr.substring(index)}`;
          this.setState({ tokenBalance: resBalance });
        break;
      }
    }
  };

  componentWillUnmount = () => {
    this.ws.close();
  };

  handleChangeInput = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClickGet = () => {
    const { tkName } = this.state;
    try {
      this.ws.send(JSON.stringify({
        "event": "getTokenInfo",
        "tkName": tkName,
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  handleClickGetBalance = () => {
    const { address } = this.state;
    try {
      this.ws.send(JSON.stringify({
        "event": "getBalance",
        "type": 2,
        "tkname": TokenName,
        "address": address,
      }));
    } catch (error) {
      const { enqueueSnackbar } = this.props;
      enqueueSnackbar(error.message, {variant: 'error'});
    }
  }

  render() {
    const { classes } = this.props;
    const { tkName, address, tkInfo, tokenBalance } = this.state;

    return (
      <div className={classes.container} data-tid="container">
        <Card>
          <CardContent>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography
                  className={classes.title}
                  color="textPrimary"
                  gutterBottom
                  inline
                >
                  Get Token Info
                </Typography>
              </Grid>
              <Grid item xs={12} sm={8}>
                <TextField
                  value={tkName}
                  onChange={this.handleChangeInput('tkName')}
                  label="Token Name"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleClickGet}
                  fullWidth
                >
                  Get
                </Button>
              </Grid>

              {tkInfo.From ?
                (
                  <React.Fragment>
                    <Grid item xs={12}>
                      <Table>
                        <TableBody>
                          {
                            Object.keys(tkInfo).map(field => (
                              <TableRow key={field}>
                                <TableCell component="th" scope="row">
                                  {field}
                                </TableCell>
                                <TableCell align="right">
                                  {field === 'Mintable' ? (tkInfo[field] === 1 ? 'True' : 'False') : tkInfo[field]}
                                </TableCell>
                              </TableRow>
                            ))
                          }
                        </TableBody>
                      </Table>
                    </Grid>
                    <Grid item xs={12} sm={6} md={5}>
                      <TextField
                        value={address}
                        onChange={this.handleChangeInput('address')}
                        label="Address"
                        fullWidth
                      />
                    </Grid>
                    <Grid 
                      container 
                      xs={12} 
                      sm={6}
                      md={3}
                      justify="center"
                      direction="column"
                    >
                      <Typography>
                        Balance: {tokenBalance}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.handleClickGetBalance}
                        fullWidth
                      >
                        Get Balance
                      </Button>
                    </Grid>
                  </React.Fragment>
                ) : ''
              }
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withSnackbar(withStyles(styles, { withTheme: true })(TokenInfo));