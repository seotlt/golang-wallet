import { CHANGE_WALLET } from '../actions/wallet';

export default function wallet(state = '', action) {
  switch (action.type) {
    case CHANGE_WALLET:
      return action.payload;
    default:
      return state;
  }
}
